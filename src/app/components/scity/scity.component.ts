import { Component, OnInit, ViewChild, Input, enableProdMode } from '@angular/core';
import { ScityService } from 'src/app/services/scity.service';
import { Scity } from 'src/app/models/scity.model';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { stringify } from '@angular/core/src/render3/util';
import { AuthenticationService } from 'src/app/services/security/authentication.service';
import { Router } from '@angular/router';
import { GetRole } from 'src/app/services/security/get-role';
import { delay } from 'q';
import { SetRole } from '../set-role';
//enableProdMode();
@Component({
  selector: 'app-scity',
  templateUrl: './scity.component.html',
  styleUrls: ['./scity.component.css']
})
export class ScityComponent extends SetRole implements OnInit {

  public data$: Scity[];
  closeResult: string;
  scity: Scity = new Scity();
  submitted = false;
  userRoleStatus : string;
 // @Input() role:string

  constructor(private service:ScityService,private authentocationService: AuthenticationService,private router: Router,public getRole:GetRole) {
    super(getRole)
  }

  ngOnInit() {
    this.getAll();
    this.authentocationService.currentUserRole.subscribe(result => {this.userRoleStatus = result});
 
    /*(async () => { 
      this.getRole.getIdFromUser(localStorage.getItem('username'));  
      await delay(1000);
      this.role=this.getRole.currentUserRole;
    })();*/
   
  
  }

  public getAll() {
    return this.service.get()
   .subscribe(data=>this.data$=data);
  }

  public delete(id: number) {
      this.service.delete(id)
        .subscribe(data => {
        console.log(data);
        this.getAll();
        },
          error => console.log(error));
        
  }  

  public refresh(): void {
      window.location.reload();
  }  
 
  //INSERT
  newValue(): void {
    this.submitted = false;
    this.scity = new Scity();
  }

  save() {
    this.service.create(this.scity)//************************************* 
      .subscribe(data => console.log(data), error => console.log(error));
    this.scity = new Scity();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  //  this.refresh();
    this.service.refreshList();
  }

  logout(){
    this.authentocationService.logOut();
    this.router.navigate(['login']);
  }


}
