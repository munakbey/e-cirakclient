import { GetRole } from '../services/security/get-role';
import { Input } from '@angular/core';
import { delay } from 'q';


export class SetRole {
    @Input() role:string
    @Input() type:string

    constructor(public getRole:GetRole) { 
        (async () => { 
            this.getRole.getIdFromUser(localStorage.getItem('username'));  
            await delay(1000);
            this.role=this.getRole.currentUserRole;
            this.type=this.getRole.currentUserType;
            console.log("++"+this.type);
          })();
         
        
    }
   
}
