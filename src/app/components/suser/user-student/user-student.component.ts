import { Component, OnInit, Input } from '@angular/core';
import { UserSchoolComponent } from '../user-school/user-school.component';
import { FormBuilder } from '@angular/forms';
import { ScityService } from 'src/app/services/scity.service';
import { SdistrictService } from 'src/app/services/sdistrict.service';
import { SneighborhoodService } from 'src/app/services/sneighborhood.service';
import { SuserService } from 'src/app/services/suser.service';
import { UserExperienceService } from 'src/app/services/user-experience.service';
import { UserExperience } from 'src/app/models/user-experience.model';
import { SUser } from 'src/app/models/suser.model';
import { CompanyService } from 'src/app/services/company.service';
import { delay } from 'q';
import { Company } from 'src/app/models/company.model';
import { GetRole } from 'src/app/services/security/get-role';

@Component({
  selector: 'app-user-student',
  templateUrl: './user-student.component.html',
  styleUrls: ['./user-student.component.css']
})
export class UserStudentComponent extends UserSchoolComponent implements OnInit {

  public userExp$: UserExperience[];
  public company$: SUser[]=[];  //Get company type users
  public company: Company[]=[]; //Get company list
  companyName:string;
  companyId:number;
  @Input() selectedCompany:any = { name: ''};
  @Input() selectedUser:any = { name: ''};
  @Input() companyById:any = { name: ''}; //show experience popup
  tmp:number;
  index:number=0;

  relatedUserId:any[]=[];
  companyList:any[]=[];

  constructor(public service:SuserService,public serviceUserExp:UserExperienceService,public serviceCompany:CompanyService,public serviceCity:ScityService,public serviceDistrict:SdistrictService,
    public serviceNeighborhood:SneighborhoodService, public fb: FormBuilder,public getRole:GetRole) {
      super(service,serviceCity,serviceDistrict,serviceNeighborhood,fb,getRole);
     }

  ngOnInit() {
    this.type="student";
    this.getAll(this.type);
    this.getSCity();
    this.getSDistrict();
    this.getSNeighborhood();
    this.getCompany();

  }

  public get() {
    return  this.service.getByType("company")
    .subscribe(data=>this.company$=data);
  } 

  public getCompany() {
    return  this.serviceCompany.get()
    .subscribe(data=>this.company=data);
  } 


  public getUserExp(id:number) {
    return this.serviceUserExp.getByUserId(id)
    .subscribe(data=>this.userExp$=data);
  }  


  getCompanyId(id:number){
    return this.service.getSuser(id).subscribe(data => {
      console.log(data);
      this.selectedUser=data; 
      this.getCompanyById(this.selectedUser.company_id);
    },error => console.log(error)); 
  }

 
  getDetails(event: Event){
   this.tmp=(Number)(event);
   (async () => { 
    // Do something before delay
    this.getUserExp(this.tmp);

    await delay(1000);

    // Do something after
    for(var i:number=0; i<this.userExp$.length; i++){
      this.relatedUserId[i]=this.userExp$[i].related_user_id; 
      console.log(this.userExp$[i].related_user_id);
    }
    for(var i:number=0; i<this.relatedUserId.length; i++){
      this.getCompanyId( this.relatedUserId[i]);  
    }  
    })();
  }  

  getCompanyById(id:number){
    return this.serviceCompany.getCompany(id).subscribe(data => {
      console.log(data);
      this.companyById=data;
      this.companyList[this.index]=data.name;
      this.index++;
    },error => console.log(error)); 
  }

}
