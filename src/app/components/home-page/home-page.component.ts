import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/security/authentication.service';
import { Router } from '@angular/router';
import { GetRole } from 'src/app/services/security/get-role';
import { SetRole } from '../set-role';
import * as CryptoJS from 'crypto-js';  
import { EncrDecrService } from 'src/app/services/security/encr-decr.service';
import { delay } from 'q';


@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent extends SetRole implements OnInit {
  userRoleStatus : string;
  dene:string;

  constructor(public authentocationService: AuthenticationService,public router: Router,public getRole:GetRole,private EncrDecr: EncrDecrService){ 
    super(getRole);
  }

  async ngOnInit() {
    //this.role.getIdFromUser("ogr1");
    this.authentocationService.currentUserRole.subscribe(result => {this.userRoleStatus = result});
    await delay(1000); 
    var encrypted = this.EncrDecr.set('123456$#@$^@1ERF', this.type);
   //var decrypted = this.EncrDecr.get('123456$#@$^@1ERF', encrypted);
    localStorage.setItem('type',encrypted)
    console.log('Encrypted :' + encrypted+"  ("+this.type);
   //console.log('Encrypted :' + decrypted);
  }

  logout(){
    this.authentocationService.logOut();
    this.router.navigate(['login']);
  }

    

}
