import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/security/authentication.service';
import { HttpErrorResponse } from '@angular/common/http';
import { GetRole } from 'src/app/services/security/get-role';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username = ''
  password = ''
  invalidLogin = false

  constructor(private router: Router,
    private loginservice: AuthenticationService,public role:GetRole) { }

  ngOnInit() {
  }

  checkLogin() {
    (this.loginservice.authenticate(this.username, this.password).subscribe(
      data => {
        console.log(this.loginservice.xx)
        localStorage.setItem('token', this.loginservice.xx)
        this.router.navigate(['/'])
        this.invalidLogin = false
      },
      error => {
        this.invalidLogin = true
        if( error instanceof HttpErrorResponse ) {
          if (error.status === 401) {
            this.router.navigate(['/login'])
          }
        }
      }
    )
    );
  }

}