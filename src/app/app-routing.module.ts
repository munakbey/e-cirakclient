import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { TableComponent } from './components/table/table.component';
import { LoginComponent } from './components/login/login.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { RegisterComponent } from './components/register/register.component';
import { DenemeComponent } from './components/deneme/deneme.component';
import { ScityComponent } from './components/scity/scity.component';
import { ScityUpdateComponent } from './components/scity/updateScity/scity-update/scity-update.component';
import { SdistrictComponent } from './components/sdistrict/sdistrict.component';
import { SdistrictUpdateComponent } from './components/sdistrict/updateSdistrict/sdistrict-update/sdistrict-update.component';
import { SneighborhoodComponent } from './components/sneighborhood/sneighborhood.component';
import { SneighborhoodUpdateComponent } from './components/sneighborhood/updateSneighborhood/sneighborhood-update/sneighborhood-update.component';
import { SchoolComponent } from './components/school/school.component';
import { SchoolUpdateComponent } from './components/school/updateSchool/school-update/school-update.component';
import { CompanyComponent } from './components/company/company.component';
import { CompanyUpdateComponent } from './components/company/company-update/company-update.component';
import { SuserComponent } from './components/suser/suser.component';
import { SuserUpdateComponent } from './components/suser/suser-update/suser-update.component';
import { PropertyComponent } from './components/property/property.component';
import { PropertyUpdateComponent } from './components/property/property-update/property-update.component';
import { StudentPropComponent } from './components/student-prop/student-prop.component';
import { StudentPropUpdateComponent } from './components/student-prop/student-prop-update/student-prop-update.component';
import { SidebarComponent } from './components/navigation/sidebar/sidebar.component';
import { TopbarComponent } from './components/navigation/topbar/topbar.component';
import { CompanyReqComponent } from './components/company-req/company-req.component';
import { CompanyReqUpdateComponent } from './components/company-req/company-req-update/company-req-update.component';
import { SpecialCaseComponent } from './components/special-case/special-case.component';
import { SpecialCaseUpdateComponent } from './components/special-case/special-case-update/special-case-update.component';
import { UserSpecialCaseComponent } from './components/user-special-case/user-special-case.component';
import { UserSpecialCaseUpdateComponent } from './components/user-special-case/user-special-case-update/user-special-case-update.component';
import { SRoleComponent } from './components/s-role/s-role.component';
import { SRoleUpdateComponent } from './components/s-role/s-role-update/s-role-update.component';
import { SUserRoleComponent } from './components/suser-role/s-user-role.component';
import { SuserRoleUpdateComponent } from './components/suser-role/suser-role-update/suser-role-update.component';
import { SrolePermissionComponent } from './components/srole-permission/srole-permission.component';
import { SrolePermissionUpdateComponent } from './components/srole-permission/srole-permission-update/srole-permission-update.component';
import { UserExperienceComponent } from './components/user-experience/user-experience.component';
import { UserExperienceUpdateComponent } from './components/user-experience/user-experience-update/user-experience-update.component';
import { UserSchoolComponent } from './components/suser/user-school/user-school.component';
import { UserSchoolUpdateComponent } from './components/suser/user-school/user-school-update/user-school-update.component';
import { UserStudentComponent } from './components/suser/user-student/user-student.component';
import { UserStudentUpdateComponent } from './components/suser/user-student/user-student-update/user-student-update.component';
import { UserCompanyComponent } from './components/suser/user-company/user-company.component';
import { UserCompanyUpdateComponent } from './components/suser/user-company/user-company-update/user-company-update.component';
import { AuthGuardService } from './services/security/auth-guard.service';


const routes: Routes = [
  { path: 'not-found'               , component: NotFoundComponent } , 
  { path: ''                        , component: HomePageComponent },
  { path: 'list'                    , component: TableComponent    },
  { path: 'login'                   , component: LoginComponent,    },
  { path: 'forgot-password'         , component: ForgotPasswordComponent},
  { path: 'register'                , component: RegisterComponent},
  { path: 'deneme'                  , component: DenemeComponent} ,                                          
  { path: 'list/deneme/:id'         , component: DenemeComponent,   data: { title: 'Edit todo' } },
  { path: 'city'                    , component: ScityComponent ,   canActivate: [AuthGuardService]} ,
  { path: 'city/update/:id'         , component: ScityUpdateComponent, canActivate: [AuthGuardService]  },
  { path: 'district'                , component: SdistrictComponent, canActivate: [AuthGuardService]} ,
  { path: 'district/update/:id'     , component: SdistrictUpdateComponent,  canActivate: [AuthGuardService]  },
  { path: 'neighborhood'            , component: SneighborhoodComponent, canActivate: [AuthGuardService]   },
  { path: 'neighborhood/update/:id' , component: SneighborhoodUpdateComponent, canActivate: [AuthGuardService]   },
  { path: 'school'                  , component: SchoolComponent, canActivate: [AuthGuardService]   },
  { path: 'school/update/:id'       , component: SchoolUpdateComponent, canActivate: [AuthGuardService]   },
  { path: 'company'                 , component: CompanyComponent,  canActivate: [AuthGuardService]  },
  { path: 'company/update/:id'      , component: CompanyUpdateComponent, canActivate: [AuthGuardService]   },
  { path: 'user'                    , component: SuserComponent, canActivate: [AuthGuardService]   },
  { path: 'user/update/:id'         , component: SuserUpdateComponent, canActivate: [AuthGuardService]   },
  { path: 'property'                , component: PropertyComponent, canActivate: [AuthGuardService]   },
  { path: 'property/update/:id'     , component: PropertyUpdateComponent, canActivate: [AuthGuardService]   },
  { path: 'student-property'        , component: StudentPropComponent, canActivate: [AuthGuardService]  },
  { path: 'student-property/update/:id', component:StudentPropUpdateComponent, canActivate: [AuthGuardService]  },
  { path: 'sidebar'                 , component: SidebarComponent,   },
  { path: 'topbar'                  , component: TopbarComponent,   },
  { path: 'company-req'             , component: CompanyReqComponent, canActivate: [AuthGuardService]  },
  { path: 'company-req/update/:id'  , component: CompanyReqUpdateComponent, canActivate: [AuthGuardService]  },
  { path: 'special-case'            , component: SpecialCaseComponent, canActivate: [AuthGuardService]  },
  { path: 'special-case/update/:id' , component: SpecialCaseUpdateComponent, canActivate: [AuthGuardService]  },
  { path: 'user-special-case'       , component: UserSpecialCaseComponent, canActivate: [AuthGuardService]  },
  { path: 'user-special-case/update/:id', component: UserSpecialCaseUpdateComponent, canActivate: [AuthGuardService]  },
  { path: 'user-experience'         , component: UserExperienceComponent, canActivate: [AuthGuardService]  },
  { path: 'user-experience/update/:id', component: UserExperienceUpdateComponent, canActivate: [AuthGuardService]  },
  { path: 'role'                    , component: SRoleComponent, canActivate: [AuthGuardService]  },
  { path: 'role/update/:id'         , component: SRoleUpdateComponent, canActivate: [AuthGuardService]  },
  { path: 'user-role'               , component: SUserRoleComponent, canActivate: [AuthGuardService]  },
  { path: 'user-role/update/:id'    , component: SuserRoleUpdateComponent, canActivate: [AuthGuardService]  },
  { path: 'role-permission'         , component: SrolePermissionComponent, canActivate: [AuthGuardService]  },
  { path: 'role-permission/update/:id', component: SrolePermissionUpdateComponent, canActivate: [AuthGuardService]  },
  { path: 'user-school'             , component: UserSchoolComponent, canActivate: [AuthGuardService]  },
  { path: 'user-school/update/:id'  , component: UserSchoolUpdateComponent, canActivate: [AuthGuardService]  },
  { path: 'user-student'            , component: UserStudentComponent, canActivate: [AuthGuardService]  },
  { path: 'user-student/update/:id'  , component:UserStudentUpdateComponent, canActivate: [AuthGuardService]  },
  { path: 'user-company'            , component: UserCompanyComponent, canActivate: [AuthGuardService]  },
  { path: 'user-company/update/:id'  , component:UserCompanyUpdateComponent, canActivate: [AuthGuardService]  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
