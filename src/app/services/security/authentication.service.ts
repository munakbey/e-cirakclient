import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { GetRole } from './get-role';
import { AuthGuardService } from './auth-guard.service';


export class User{
  constructor(
    public status:string,
     ) {}
  
}

export class JwtResponse{
  constructor(
    public jwttoken:string,
     ) {}
  
}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
    [x: string]: any;
 
  public xx : string;
     // User related properties
     private loginStatus = new BehaviorSubject<boolean>(true);
     private UserName    = new BehaviorSubject<string>(localStorage.getItem('username'));
     private UserRole    = new BehaviorSubject<string>(localStorage.getItem('userRole'));
     private userRoleStatus    = new BehaviorSubject<string>('User');
    
  constructor(private httpClient:HttpClient,public role:GetRole) { }

    authenticate(username, password) {
      return this.httpClient.post<any>('http://localhost:8883/authenticate',{username,password}).pipe(
       map(
         userData => {
          sessionStorage.setItem('username',username);
          let tokenStr= 'Bearer '+userData.token;
          this.xx=userData.token;
          console.log((String)(this.UserName)+"****");
          sessionStorage.setItem('token', tokenStr);
          localStorage.setItem('username',username);
          return userData;
         }
       )
      );   
  }

  isUserLoggedIn() {
    let user = sessionStorage.getItem('username')
    //console.log(!(user === null))
    return !(user === null)
  }

  logOut() {
     localStorage.clear();
     localStorage.setItem('token','');
     sessionStorage.removeItem('token')
  }

  getToken() {
    return localStorage.getItem('token')
  }

  get isLoggesIn() 
  {
      return this.loginStatus.asObservable();
  }

 public get currentUserRole() 
  {
      return this.UserRole.asObservable();
  }
  

  
}
