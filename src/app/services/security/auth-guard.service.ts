import { Injectable, Input } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { take, map, delay } from 'rxjs/operators';
import { AuthenticationService } from './authentication.service';
import { GetRole } from 'src/app/services/security/get-role';
import { EncrDecrService } from 'src/app/services/security/encr-decr.service';
import { SuserService } from '../suser.service';
import { SUserRoleService } from '../s-user-role.service';
import { SRoleService } from '../s-role.service';
@Injectable({
  providedIn: 'root'
})
export class AuthGuardService   implements CanActivate  {

  public id:number;
  @Input() selectedUser:any = { name: ''};
  @Input() selectedUserType:any = { name: ''};

   constructor(public authentocationService: AuthenticationService,public router: Router,public getRole:GetRole,
    public service:SuserService,public serviceUserRole:SUserRoleService,public serviceRole:SRoleService,private EncrDecr: EncrDecrService ) {
    //  this.getIdFromUser(localStorage.getItem('username'));  
  }

  canActivate( route: ActivatedRouteSnapshot, state : RouterStateSnapshot) : Observable<boolean> 
  {

    var decrypted = this.EncrDecr.get('123456$#@$^@1ERF', localStorage.getItem('type'));
    const type=decrypted;
    console.log(decrypted+"   hadiii");
   
      return this.authentocationService.isLoggesIn.pipe(take(1), map((loginStatus : boolean) => 
      {   
            const destination: string  = state.url;
            const productId = route.params.id; 


    if(type=='student'){ 
      switch(destination) 
      {      
        case '/city' :
        case '/district':
        case '/neighborhood':
        case '/s-role':
        case '/school':
        case '/special-case':
        case 'srole-permission':
        case '/student-prop':
        case '/suser':
        case '/suser-role':
        case '/user-experience':
        case '/user-special-case':                  
        case '/property':{
          this.router.navigate(['/not-found'])
        }
      }
    }
    if(type=='school'){    
      switch(destination) 
      {      
        case '/city' :
        case '/district':
        case '/neighborhood':
        case '/s-role':
        case 'srole-permission':
        case '/student-prop':
        case '/user-school':
        case '/suser-role':{
          this.router.navigate(['/not-found'])
        }
      }
    }




          // if the user is already logged in
        /*   switch(destination) 
          {
          //   console.log("bakim4");
          //    case '/city/' + productId :
           case '/city' :
                {console.log("bakim2 "+productId);
                      if(localStorage.getItem("username") === "") 
                      { console.log("bakimx");
                          return false;
                      }else{
                          console.log("bakim3");
                        this.router.navigate(['/city'])

                        return true;
                      }
              }*/

           /*  case '/city' : 
              {console.log("alandayız?");
                      if(localStorage.getItem("username") === "" ) 
                      {
                          console.log("bakim1");
                          this.router.navigate(['/login'])

                          return false;
                       }else{
                           console.log("abiii");
                       }
                     /*  if(localStorage.getItem("username") === "ogr1"){
                           console.log("girdi");
                        //   this.router.navigate(['/city']);
                       }*/
                    
               /*  if(localStorage.getItem("userRole") === "Admin") 
                      {
                        console.log("bakim");
                          return true;
                      }

              }
              case  '/city/' + productId :{
                console.log("ne alaka"+productId);
                this.router.navigate(['/city']);
              //  return true;
              }*/

            /* default:   console.log("noldu");
                  return true;
        }*/           
         return true; 
      }));

    
  }

}
