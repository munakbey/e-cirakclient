import { SuserService } from '../suser.service';
import { Input, Injectable } from '@angular/core';
import { SUserRoleService } from '../s-user-role.service';
import { SRoleService } from '../s-role.service';

@Injectable({
    providedIn: 'root'
  })
export class GetRole {

    public id:number;
    @Input() selectedUser:any = { name: ''};
    @Input() selectedUserType:any = { name: ''};
    public  role:string;
    public type:string;

    constructor(public service:SuserService,public serviceUserRole:SUserRoleService,public serviceRole:SRoleService){}

    public getIdFromUser(username:string){
        return this.service.getByUsername(username).subscribe(data => {
      //    console.log(data);
          console.log("sa");
            this.selectedUser=data; 
            this.getRoleId((data));   
            this.getUserType(data);
          },error => console.log(error)); 
    }

    public getRoleId(id:number){
      return this.serviceUserRole.getRoleId(id).subscribe(data => {
  //      console.log(data);
        this.getRoleById(data);
    //    console.log(this.selectedUser.role+"***");
        },error => console.log(error)); 
    }

    public getRoleById(id:number){
      return this.serviceRole.getRoleById(id).subscribe(data => {
   //     console.log(data);
        this.selectedUser=data;
        this.role=this.selectedUser.roleName;
        console.log(this.role+"___!##");     
      //  localStorage.setItem('userRole', this.role); 
        },error => console.log(error)); 
    }

    public getUserType(id:number){     
      return this.service.getTypeFromId(id).subscribe(data => {
     //   console.log(data);
        this.selectedUserType=data;
        this.type=this.selectedUserType.type;
        console.log(this.type+"&&&&&&&&&&&")
        },error => console.log(error));
    }

    public  get currentUserRole(): string { 
      return this.role; 
    }

    public  get currentUserType(): string { 
      return this.type; 
    }

    
}
